# Copyright 2015 Hewlett-Packard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

import unittest
from mock import Mock, patch

from freezer.scheduler import arguments


class TestBaseParser(unittest.TestCase):

    def test_returns_parser(self):
        mock_parser = Mock()
        mock_parser._me = 'test12345'
        retval = arguments.base_parser(mock_parser)
        self.assertEquals(retval._me, 'test12345')

        call_count = mock_parser.add_argument.call_count
        self.assertGreater(call_count, 10)

    @patch('freezer.scheduler.arguments.base_parser')
    def test_get_args_return_parsed_args(self, mock_base_parser):
        mock_parser = Mock()
        mock_parser.parse_args.return_value = 'pluto'
        mock_base_parser.return_value = mock_parser
        retval = arguments.get_args(['alpha', 'bravo'])
        call_count = mock_parser.add_argument.call_count
        self.assertGreater(call_count, 0)
